<section class="content_section page_title">
    <div class="content clearfix">
        <h1 class=""><?php single_post_title(); ?></h1>
        <?php if (function_exists('kyma_breadcrumbs')) kyma_breadcrumbs(); ?>
    </div>
</section>