=== Popular Brand SVG Icons - Simple Icons ===
Contributors: tjtaylor
Donate link: https://simpleicons.org/
Tags: social media icons, menu icons, svg icons, icons shortcodes, add social media icons to wordpress
Requires at least: 2.8
Tested up to: 5.2.3
Requires PHP: 5.2.4
Stable tag: 2.3.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

An easy to use SVG icons plugin with over 500+ brand icons. Use these icons in your menus, widgets, posts, or pages.

== Description ==

Add popular brand icons to WordPress with ease. Use these high quality SVG icons anywhere on your WordPress site, set the color and size using attributes.

<a title="WordPress Simple Icons Tutorial" href="http://www.youtube.com/watch?v=4GrHU-hyXs0" target="_blank">**Video Tutorial**</a>

== How to use ==

[youtube http://www.youtube.com/watch?v=4GrHU-hyXs0]

Select an icon from the WordPress admin settings page, use the hashtag name in menus or use the shortcode for pages or php files.

Example:

`[simple_icon name="wordpress" color="black" size="20px"]`

Properties:

*	<strong>name</strong>
	The name of the social icon (search [https://simpleicons.org/](https://simpleicons.org) to find an icon).

*	<strong>color</strong> (optional)
	The color of the icon (example: #ffffff or white).
	<em>Default is pulled from the framework</em>

*	<strong>size</strong> (optional)
	The size of the icon
	<em>Default is 1.5rem</em>

*	<strong>class</strong> (optional)
	Add a custom CSS class to the icon.


== How to style icons ==

Use the following CSS code to change all icons color, size, etc.

`span[class*="simple-icon"] {
	width: 2em;
	height: 2em;
}
`

`span[class*="simple-icon"] svg {
	fill: #333;
}
`

== Notes ==

Visit [https://simpleicons.org](https://simpleicons.org) to view over 500+ available social media icons and popular brand icons. Use the shortcode [simple_icon] to display an icon or use #iconname# in your menu item title.

Use [GitHub](https://github.com/simple-icons/simple-icons) for social media or other icon requests, corrections and contributions.

Please post any issues under the support tab. If you use and like this plugin, please don’t forget to <strong>rate</strong> it! Additionally, if you would like to see more features for the plugin, please let me know.


== Icons ==

Icons include: (672 total icons) .NET icon, Arduino icon, 500px icon, About.me icon, ACM icon, AddThis icon, Adobe icon, Adobe Acrobat Reader icon, Adobe After Effects icon, Adobe Audition icon, Adobe Dreamweaver icon, Adobe Illustrator icon, Adobe InDesign icon, Adobe Lightroom CC icon, Adobe Lightroom Classic icon, Adobe Photoshop icon, Adobe Premiere icon, Adobe Typekit icon, Adobe XD icon, Airbnb icon (social media icon), AlloCiné icon, Amazon icon, Amazon AWS icon, AMD icon, American Express icon, Android icon, AngelList icon, Angular icon, Angular Universal icon, Ansible icon, Apache icon, Apache Flink icon, Apple icon, Apple Music icon, Apple Pay icon, AppVeyor icon, ARAL icon, Arch Linux icon, Archive of Our Own icon, ArtStation icon, Asana icon, Atlassian icon, Atom icon, AT&T icon, Audible icon, Aurelia icon, Auth0 icon, Automatic icon, Autotask icon, Aventrix icon, Azure DevOps icon, Azure Pipelines icon, Babel icon, Baidu icon, Bamboo icon, Bandcamp icon, Basecamp icon, Bath ASU icon, Beats icon, Behance icon, Big Cartel icon, Bing icon, Bit icon, Bitbucket icon, Bitcoin icon, Bitdefender icon, Bitly icon, Blender icon, Blogger icon, Boeing icon, Boost icon, Bootstrap icon, Bower icon, Brand.ai icon, Brave icon, Buddy icon, Buffer icon, Buy Me A Coffee icon, BuzzFeed icon, CakePHP icon, Campaign Monitor icon, Canva icon, Cash App icon, Castorama icon, Castro icon, Cassandra icon, CEVO icon, Chase icon, Chef icon, Circle icon, CircleCI icon, Cirrus CI icon, CiviCRM icon, Clockify icon, Clojure icon, CloudBees icon, Cloudflare icon, CMake icon, Codacy icon, Codecademy icon, Code Climate icon, Codecov icon, Codeforces icon, CodeIgniter icon, CodePen icon, Coderwall icon, CodeSandbox icon, Codeship icon, Codewars icon, Codio icon, CoffeeScript icon, Composer icon, ComproPago icon, Conekta icon, Confluence icon, Conda-Forge icon, Co-op icon, Coursera icon, Adobe Creative Cloud icon, Creative Commons icon, Crunchbase icon, Crunchyroll icon, CSS3 icon, CSS Wizardry icon, Common Workflow Language icon, C++ icon, D3.js icon, Dailymotion icon, Dashlane icon, Dassault Systèmes icon, DAZN icon, dblp icon, Debian icon, Deezer icon, Delicious icon, Dell icon, Deno icon, Dependabot icon, Designer News icon, DeviantArt icon, devRant icon, dev.to icon, Diaspora icon, Digg icon (social media icon), DigitalOcean icon, Discord icon, Discourse icon, Discover icon, Disroot icon, Disqus icon, Django icon, Docker icon, DocuSign icon, Draugiem.lv icon, Dribbble icon (social media icon), Drone icon, Dropbox icon, Drupal icon, DTube icon, DuckDuckGo icon, Dynatrace icon, eBay icon, EVRY icon, Exercism icon, Eclipse IDE icon, Elastic icon, Elastic Cloud icon, Elasticsearch icon, Elastic Stack icon, Electron icon, elementary icon, Ello icon, Elsevier icon, Empire Kred icon, Envato icon, Epic Games icon, ESEA icon, ESLint icon, Ethereum icon, Etsy icon, Eventbrite icon, Event Store icon, Evernote icon, Everplaces icon, Experts Exchange icon, F-Secure icon, Facebook icon (social media icon), FACEIT icon, Fandango icon, Favro icon, FeatHub icon, Fedora icon, Feedly icon, Fido Alliance icon, FileZilla icon, Firebase icon, Fitbit icon, Flask icon, Flattr icon, Flickr icon (social media icon), Flipboard icon, Flutter icon, Fnac icon, Foursquare icon (social media icon), Figma icon, Framer icon, FreeBSD icon, freeCodeCamp icon, Fur Affinity icon, Furry Network icon, Garmin icon, Gatsby icon, Gauges icon, Geocaching icon, Gerrit icon, Ghost icon, Git icon (social media icon), GitHub icon, GitLab icon, Gitpod icon, Gitter icon, Glassdoor icon, Gmail icon, GNU icon, GNU social icon, Godot Engine icon, GOG.com icon, GoldenLine icon, Goodreads icon, Go icon, Google icon, Google Allo icon, Google Chrome icon, Google Cloud icon, Google Analytics icon, Google Drive icon, Google Hangouts icon, Google Hangouts Chat icon, Google Keep icon, Google Pay icon, Google Play icon, Google Podcasts icon, GOV.UK icon, Grafana icon, Graphcool icon, GraphQL icon, Grav icon, Gravatar icon, Greenkeeper icon, Groupon icon, Gulp icon, Gumroad icon, Gumtree icon, Hackaday icon, HackerEarth icon, HackerRank icon, HackHands icon, Hackster icon, Hashnode icon, Haskell icon, Hatena Bookmark icon, Haxe icon, Helm icon, HERE icon, Heroku icon, Hexo icon, Highly icon, HipChat icon, HockeyApp icon, homify icon, Hootsuite icon, Houzz icon, HTML5 icon, Huawei icon, HubSpot icon, Hulu icon, Humble Bundle icon, Iata icon, iCloud icon, Ionic icon, IconJar icon, ICQ icon, iFixit icon, IMDb icon, Inkscape icon, Instacart icon, Instagram icon (social media icon), Instapaper icon, Intel icon, IntelliJ IDEA icon, Intercom icon, Internet Explorer icon, InVision icon, Itch.io icon, Jabber icon, Java icon, JavaScript icon, Jekyll icon, Jenkins icon, Jest icon, Jira icon, Joomla icon, jQuery icon, jsDelivr icon, JSFiddle icon, JSON icon, Jupyter icon, JustGiving icon, Kaggle icon, KaiOS icon, Kentico icon, Keybase icon, KeyCDN icon, Khan Academy icon, Kibana icon, Kickstarter icon, Kik icon, Kirby icon, Klout icon, Known icon, Kodi icon, Koding icon, Kotlin icon, Ko-fi icon, Kubernetes icon, Lanyrd icon, Laravel icon, Laravel Horizon icon, Last.fm icon, Launchpad icon, LeetCode icon, Let’s Encrypt icon, Letterboxd icon, LGTM icon, LibraryThing icon, Liberapay icon, Line icon, LINE WEBTOON icon, LinkedIn icon (social media icon), Linux icon, Linux Foundation icon, LiveJournal icon, Livestream icon, Logstash icon, Lua icon, Lyft icon, Macy’s icon, Magento icon, Magisk icon, MailChimp icon, Mail.Ru icon, MakerBot icon, Manjaro icon, Markdown icon, Marketo icon, MasterCard icon, Mastodon icon, Material Design icon, Mathworks icon, Matrix icon, Matternet icon, MediaFire icon, MediaTemple icon, Medium icon, Meetup icon (social media icon), MEGA icon, Messenger icon, Meteor icon, Micro.blog icon, Microgenetics icon, Microsoft icon, Microsoft Access icon, Microsoft Azure icon, Microsoft Edge icon, Microsoft Excel icon, Microsoft OneDrive icon, Microsoft OneNote icon, Microsoft Outlook icon, Microsoft PowerPoint icon, Microsoft Word icon, Linux Mint icon, Minutemailer icon, Mix icon, Mixcloud icon, Mixer icon, Monero icon, MongoDB icon, Monkey tie icon, Monogram icon, Monzo icon, Moo icon, Mozilla icon, Mozilla Firefox icon, MX Linux icon, Myspace icon, MySQL icon, NativeScript icon, Neo4j icon, Netflix icon, Netlify icon, Next.js icon, Nextcloud icon, Nextdoor icon, NGINX icon, Nintendo icon, Nintendo GameCube icon, Nintendo Switch icon, Node.js icon, Nodemon icon, NPM icon, Nucleo icon, NuGet icon, Nuxt.js icon, NVIDIA icon, OCaml icon, Octopus Deploy icon, Oculus icon, Odnoklassniki icon, Open Access icon, OpenStreetMap icon, openSUSE icon, OpenVPN icon, Opera icon, Opsgenie icon, Oracle icon, ORCID icon, Origin icon, OSMC icon, Overcast icon, OVH icon, Pagekit icon, Pandora icon, Pantheon icon, Patreon icon, PayPal icon, Periscope icon (social media icon), PHP icon, Picarto.TV icon, Pinboard icon, Pingdom icon, Pingup icon, Pinterest icon (social media icon), Pivotal Tracker icon, PlanGrid icon, Player.me icon, PlayStation icon, PlayStation 3 icon, PlayStation 4 icon, Plex icon, Pluralsight icon, Plurk icon, Pocket icon, Podcasts icon (social media icon), PostgreSQL icon, Postman icon, PowerShell icon, Prettier icon, Prismic icon, Probot icon, ProcessWire icon, Product Hunt icon, Proto.io icon, ProtonMail icon, Proxmox icon, PyPI icon, Python icon, Qiita icon, Qualcomm icon, Quantopian icon, Quantcast icon, Quora icon (social media icon), Qwiklabs icon, Qzone icon, R icon, Rails icon, Raspberry Pi icon, React icon, Read the Docs icon, Reason icon, Redbubble icon, Reddit icon (social media icon), Red Hat icon, Redis icon, Redux icon, Renren icon, ReverbNation icon, Riot icon, Riseup icon, rollup.js icon, Roots icon, RSS icon, Ruby icon, RubyGems icon, Runkeeper icon, Rust icon, Safari icon, Salesforce icon, Samsung icon, Samsung Pay icon, SAP icon, Sass icon, Sauce Labs icon, Scala icon, Scaleway icon, Scribd icon, Scrutinizer CI icon, Sega icon, Sellfy icon, Sensu icon, Sentry icon, Server Fault icon, Shazam icon, Shell icon, Shopify icon, Signal icon, Simple Icons icon, Sina Weibo icon, SitePoint icon, Skyliner icon, Skype icon, Slack icon, Slashdot icon, SlickPic icon, Slides icon, Smashing Magazine icon, Snapchat icon (social media icon), Snapcraft icon, Snyk icon, Society6 icon, Sogou icon, Solus icon, Songkick icon, SoundCloud icon, SourceForge icon, Sourcegraph icon, Spacemacs icon, SpaceX icon, SparkFun icon, SparkPost icon, SPDX icon, Speaker Deck icon, Spotify icon, Spotlight icon, Spreaker icon, Sprint icon, Squarespace icon, Stack Exchange icon, Stack Overflow icon, StackShare icon, Stadia icon, Statamic icon, Statuspage icon, Staticman icon, Steam icon, Steem icon, Steemit icon, Stitcher icon, Storify icon, Strava icon, Stripe icon, StubHub icon, StyleShare icon, Stylus icon, Sublime Text icon, Subversion icon, Super User icon, Swarm icon, Swift icon, Symfony icon, Synology icon, Tails icon, Tapas icon, TeamViewer icon, TED icon, Teespring icon, Telegram icon, Tencent QQ icon, Tencent Weibo icon, Tesla icon, The Mighty icon, The Movie Database icon, Tidal icon, Tik Tok icon, Tinder icon (social media icon), T-Mobile icon, Todoist icon, Toggl icon, Topcoder icon, Toptal icon, Tor icon, TrainerRoad icon, Trakt icon, Travis CI icon, Treehouse icon, Trello icon, TripAdvisor icon, Trulia icon, Tumblr icon (social media icon), Turkish Airlines icon, Twilio icon, Twitch icon, Twitter icon (social media icon), Twoo icon, TypeScript icon, TYPO3 icon, Uber icon, Ubisoft icon, Ubuntu icon, Udacity icon, Udemy icon, UIkit icon, Umbraco icon, Unity icon, Unreal Engine icon, Unsplash icon, Untappd icon (social media icon), Upwork icon, V8 icon, Vagrant icon, Venmo icon, Verizon icon, Viadeo icon, Viber icon, Vim icon, Vimeo icon, Vine icon (social media icon), Virb icon, Visa icon, Visual Studio Code icon, VK icon, VLC media player icon, VSCO icon, Vue.js icon, Wattpad icon, Weasyl icon, Webpack icon, webcomponents.org icon, WebStorm icon, WeChat icon, WhatsApp icon (social media icon), When I Work icon, Wii icon, Wii U icon, Wikipedia icon, Windows icon, Wire icon, WireGuard icon, Wix icon, Wolfram icon, Wolfram Language icon, Wolfram Mathematica icon, WordPress icon, WP Engine icon, Xbox icon, Xcode icon, Xero icon, Xing icon, X-Pack icon, XSplit icon, Y Combinator icon, Yahoo! icon, Yammer icon, Yandex icon, Yarn icon, Yelp icon, YouTube icon (social media icon), Zapier icon, Zeit icon, Zendesk icon, Zerply icon, Zillow icon, Zorin icon


== Contributors ==

[Simple Icon Contributors](https://github.com/simple-icons/simple-icons/graphs/contributors)


== Screenshots ==

1. Use social media icons in menu titles
2. Use social media icons as shortcode in your content
3. Highlight shortcode and click on the link button to create svg icon links
4. Example of front end use.


== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `simple-icons.zip` to the `/wp-content/plugins/` directory
2. Unzip the file
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Use the shortcode [simple_icon] anywhere in your content, or use #example# in your menu item title


== Changelog ==

= 2.3.0 =
* Remove "icon" from title tag
le-icons.zip` to the `/wp-content/plugins/` directory
2. Unzip the file
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Use the shortcode [simple_icon] anywhere in your content, or use #example# in your menu item title


== Changelog ==

= 2.3.0 =
* Remove "icon" from title tag
