<?php
use \BinaryCarpenter\BC_MNC\BC_Options_Form as Options_Form;
use \BinaryCarpenter\BC_MNC\BC_Menu_Cart_Config as BConfig;
use \BinaryCarpenter\BC_MNC\BC_Cart_Options_name as OName;
use \BinaryCarpenter\BC_MNC\BC_Options as BC_Options;
use BinaryCarpenter\BC_MNC\Activation as Activation;
use BinaryCarpenter\BC_MNC\BC_Static_UI as Static_UI;
use BinaryCarpenter\BC_MNC\BinaryCarpenterCore as Core;
class BC_MenuCart_General_Settings {

    const OPTION_NAME = 'bc_menu_bar_cart_option_name';
    public function __construct() {
        add_action( 'admin_init', array( &$this, 'init_settings' ) ); // Registers settings
        add_action( 'admin_menu', array( &$this, 'add_menu_to_bc') );
    }

    /**
     * User settings.
     */



    /**
     * Add menu page
     */
    public function add_menu_to_bc() {

        $core = new Core();
        $core->admin_menu();
        add_submenu_page(
            Core::MENU_SLUG,
            __( BConfig::PLUGIN_NAME, 'bc-menu-cart-woo' ),
            __( '<img style="width: 14px; height: 14px;" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAACXBIWXMAAH6MAAB+jAH2GftsAAA4JmlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxMzggNzkuMTU5ODI0LCAyMDE2LzA5LzE0LTAxOjA5OjAxICAgICAgICAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIKICAgICAgICAgICAgeG1sbnM6cGhvdG9zaG9wPSJodHRwOi8vbnMuYWRvYmUuY29tL3Bob3Rvc2hvcC8xLjAvIgogICAgICAgICAgICB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIKICAgICAgICAgICAgeG1sbnM6c3RFdnQ9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZUV2ZW50IyIKICAgICAgICAgICAgeG1sbnM6dGlmZj0iaHR0cDovL25zLmFkb2JlLmNvbS90aWZmLzEuMC8iCiAgICAgICAgICAgIHhtbG5zOmV4aWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20vZXhpZi8xLjAvIj4KICAgICAgICAgPHhtcDpDcmVhdG9yVG9vbD5BZG9iZSBQaG90b3Nob3AgQ0MgMjAxNyAoV2luZG93cyk8L3htcDpDcmVhdG9yVG9vbD4KICAgICAgICAgPHhtcDpDcmVhdGVEYXRlPjIwMTktMDUtMDlUMDk6MDE6MTgrMDc6MDA8L3htcDpDcmVhdGVEYXRlPgogICAgICAgICA8eG1wOk1vZGlmeURhdGU+MjAxOS0wNS0wOVQwOTowMTo0NyswNzowMDwveG1wOk1vZGlmeURhdGU+CiAgICAgICAgIDx4bXA6TWV0YWRhdGFEYXRlPjIwMTktMDUtMDlUMDk6MDE6NDcrMDc6MDA8L3htcDpNZXRhZGF0YURhdGU+CiAgICAgICAgIDxkYzpmb3JtYXQ+aW1hZ2UvcG5nPC9kYzpmb3JtYXQ+CiAgICAgICAgIDxwaG90b3Nob3A6Q29sb3JNb2RlPjM8L3Bob3Rvc2hvcDpDb2xvck1vZGU+CiAgICAgICAgIDx4bXBNTTpJbnN0YW5jZUlEPnhtcC5paWQ6MDFkNjEwMDUtMjYyNi1kZDRiLWE5ZTMtZWZjOTg3OWE2NjU1PC94bXBNTTpJbnN0YW5jZUlEPgogICAgICAgICA8eG1wTU06RG9jdW1lbnRJRD54bXAuZGlkOjAxZDYxMDA1LTI2MjYtZGQ0Yi1hOWUzLWVmYzk4NzlhNjY1NTwveG1wTU06RG9jdW1lbnRJRD4KICAgICAgICAgPHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD54bXAuZGlkOjAxZDYxMDA1LTI2MjYtZGQ0Yi1hOWUzLWVmYzk4NzlhNjY1NTwveG1wTU06T3JpZ2luYWxEb2N1bWVudElEPgogICAgICAgICA8eG1wTU06SGlzdG9yeT4KICAgICAgICAgICAgPHJkZjpTZXE+CiAgICAgICAgICAgICAgIDxyZGY6bGkgcmRmOnBhcnNlVHlwZT0iUmVzb3VyY2UiPgogICAgICAgICAgICAgICAgICA8c3RFdnQ6YWN0aW9uPmNyZWF0ZWQ8L3N0RXZ0OmFjdGlvbj4KICAgICAgICAgICAgICAgICAgPHN0RXZ0Omluc3RhbmNlSUQ+eG1wLmlpZDowMWQ2MTAwNS0yNjI2LWRkNGItYTllMy1lZmM5ODc5YTY2NTU8L3N0RXZ0Omluc3RhbmNlSUQ+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDp3aGVuPjIwMTktMDUtMDlUMDk6MDE6MTgrMDc6MDA8L3N0RXZ0OndoZW4+CiAgICAgICAgICAgICAgICAgIDxzdEV2dDpzb2Z0d2FyZUFnZW50PkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE3IChXaW5kb3dzKTwvc3RFdnQ6c29mdHdhcmVBZ2VudD4KICAgICAgICAgICAgICAgPC9yZGY6bGk+CiAgICAgICAgICAgIDwvcmRmOlNlcT4KICAgICAgICAgPC94bXBNTTpIaXN0b3J5PgogICAgICAgICA8dGlmZjpPcmllbnRhdGlvbj4xPC90aWZmOk9yaWVudGF0aW9uPgogICAgICAgICA8dGlmZjpYUmVzb2x1dGlvbj44MjI4NTgwLzEwMDAwPC90aWZmOlhSZXNvbHV0aW9uPgogICAgICAgICA8dGlmZjpZUmVzb2x1dGlvbj44MjI4NTgwLzEwMDAwPC90aWZmOllSZXNvbHV0aW9uPgogICAgICAgICA8dGlmZjpSZXNvbHV0aW9uVW5pdD4yPC90aWZmOlJlc29sdXRpb25Vbml0PgogICAgICAgICA8ZXhpZjpDb2xvclNwYWNlPjY1NTM1PC9leGlmOkNvbG9yU3BhY2U+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj42NDwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj42NDwvZXhpZjpQaXhlbFlEaW1lbnNpb24+CiAgICAgIDwvcmRmOkRlc2NyaXB0aW9uPgogICA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAgICAgICAgICAgIAo8P3hwYWNrZXQgZW5kPSJ3Ij8+KMQV7QAAACBjSFJNAAB6JQAAgIMAAPn/AACA6QAAdTAAAOpgAAA6mAAAF2+SX8VGAAAKMUlEQVR42uybe1SUZR7H3z/a/xJTM1Nk01LLQvOCIcOAtqctPeYtbzCUdtnqtGeTrbOetrZt191ya3dPh0IFEVEuc1MHGIa5v4OCFomKJLwDA2aZZqElwiAqt8/+IUygDIwyuKMx53zPnDPvzLzz+zy/5/d7ni88AiDIRHVPCpKJ6jdlonqPTFSflonqRpmodstEdUOAqlEmqutlovq4TFRny0T1M17i8ggQBC8AXpeJam4DfScT1ZHXCyDnNgm+q97wFYD6Ngy+Uy/3BeCV2zj4Tj3oDcCvZKL6218AgCxvABT+uklkx/NUSwYzbVnIHZpAAtAsE9XBPQHY5q+bRDk0TDSmEWbLYoYti1k2JXIxoCDE9ATgkD++PNqhZUTOJmKK8wHYf/Y7Qs3phNmyAgnA33sCcLa/XzynQMu9+mQe37OTi20tAFQ31DHNmskMa2YgAUjoCcCP/R35cYathFrSOdnkBqAdWPV5Pvfnp3rqQoDo054A1Pb2oXC7ighR5XXOTzbtYGxeCiU//UDn47VDIkOzNxIVWEXQawbUekvrqZYMxualMDYvhRm2LKIcWs91uUPDdGsmw3M2kXOqxhP8B84DDNEldusKXT8TKaoZn5/KqNwkgvVb/KLRuckE67cQZlMS3eU39gvAZNN2Vn2ezzH3edK/lhit38I4QyrRDi2RooZwu4ohukQ+rS71BJ/xjZOh2RsJs2UR6aX6T7Nm8NwXZt4r/4x1ZYV+0V+O7mNtaQGzRRWP2ZT+AXCvPpl/Sl94gjtSd4YZtiyGZ29C7tAQpNvIurIiz/U9td8ySp9MqCXda+r/2rCVVw/aGahH6lfljMpN8g+A6dZMZtqysHz/tecGF1pbeKnEhpD1L+KKTZ7XXQ3nmGhMY4JxW68pONGYxorPDLhbmgG41NbqF7XSDsA/KooJyUvxD4Boh5aHzTsYlrOJD50lnmDb2ttJ/aqcancdAHXNlwi3qwjO29LX/CPaoWWKJZ2ZtizmF2bzxJ7dftG8vdnIRDWPmHf0tfpMuK4uEOXQEGZTcqcukSX79Zy51HRN2i3al8vdOZuJLtD6VInlDg3TrJlMNm3nEfMOv+hh03amWNKJ6KHw9gtAZyWPcmi4JzeJScY0CmpPdmt3QbqAbHf+A+BJ3wItk4xpBOelsMF5gLWlBYztmG+Rt86O8MYBdE6JcLuKUbnJhORt7ejrmltpS9w/ALeBBhZAgPsBAw8gyqFhQmD7AQMHoNMPiC02BrIfMDAAfvYDdnGprfWW8wNqf0F+QMKA+AEheSkc/CX7Abmnjg36AYN+wKAfMOgHDPoBg37AoB8w6AcM+gE3S5GiinBRwyxRS6SXleptDUAuqphu30WoXUeEqPF1Gt4cALNsSsLtqgGrDXJRRYjFwFuffczmA+sJtuQj9y0LBhbAnAItD5m2Mz4/lYdM25ltVw1IjZgtahhtNlFS9hpUP81sUctkW7YvwAcOQKcn8NReHXXNl9h1sppQ8w4esyv9PvpjLEaeK0wG1xKoeZLEA+sZbrb6kgUDAyDaoeU+w1YetWbw/cVGAI6565gxAIZIuKgh2GLEcSQeqhdA5XLqnArkDhUPWnP8vxDypTU+ZNrOfYatHKmr9SyS1pYWMM6Q6vfRH20x8kLRZnAt4rIzhgYpDmrmk1TyN4abLX1lwfUDiOhrWWvJYETOZvJPH/cEv76imCG6RL/7AcNzkgky6Cj+8sroN0hxNEgKqFyG2xnDXIeSSdbc3rIgIVJUCeGixjdDZJr1SnAheSlE2FXdNhqRoppwu5Ig3UY213zpCT7teAVDdRuZZVP2WABvzA8oYl1ZES+XH+OToxqQnuKiFINbUuCWFB1ZMI/Ukr8y1GQjXNT0OHDRojJhqn23cL9V3zeAqZYMFhTlINae4H3pAMOzNzHZvKPDELkCYIgukXeO7vMEb//hBCNzk5hqSe9xV+YXP+D0Bi4cmUODFOcB4JYUULmci85VzCtIZ5jJyhiLkXstRsZa8nnAqudR227GWfMS7rMaBM3Bd3wzRN7vYogYTh9nnCGVMfotPF6wk2HZm1j9hdlz3Vn/E/fnb2OiMc3rttibH9Dc1kpTWxsnmuFsC9DeCi1noEmi/fxeWs9oafkuictfraOxfHG3wLtlQeUyTlaswXz4T2QefJf/FG/gzf0JxBWmECFqmGnflbD3SLxAzbxuAM709GOnmNOZW7CT8vNnPUGeuNDAvMJshKwPeapQR0t7GwA/Xr5ImE1JSF5Kr56ANz9gpsPA8/t38I30B047f8+5yldpklbRUDaf+tK51B+OpP6wnPqy33YE/KxXCK1Vy8D19JUOUb0QqpbQ5lzJqfIXOVWxpg7X4tMXJMXqrgB+8lYDJhjTCNZvQX2iygPB3dLM20f3c/jczxV/QVE2I3M3M8ehvSE/4AFTJuNNSjKL48E1m9aKJzl/dBnuilW4pdgeg/VFjZKCJimWVmcMVC2DmvngWlhD1dJRXQEc623EQs3p3KlLJL50Dy3t7ddMyd8dtHVY4Nobb6Ud6/qhZisffP7RldGrWkZ9xbM3HHzXrLjojAHXYg6XvWazHX5TkL58pdsUyO1rxCJEFXdlb0Tu0OBqOHdNu/OHJyAXVYTZdzLUZOXt/R+DayFULe0XhHopjsvOVVAzn+xDbxFiMbweYjEIYXZtNwDv+PJX32iHlpC8FCYYt6E6Ucm/Kw8yNi/Fa7u7UQjhooYgk5XX9yWCazG4ltwQhHopjubKlVAzn8ySdxlpNjPZlh0eIaqF6fZd3QA8fD1L3hnWLEbkbGaMfgsRHRni731+hKhhqNnKC4XJULX0ujOhQYrjsnMlVC8g6cB6hpmtTLHvPhIlqryeGMkILLPjyvNws4WVe1Opl+KgcoXPAC45Y6ByGR8Vb2CY2cp0+y6iRNXq3o7MjOzvP00PBIRwUctIs5m9pfHgWuQzgE5YMlHDeKseuajK8+XQVHiguT6hNh2/cWTxffkaqFzuM4AmKRZci3mhKIk7jAVlU2y7fxVq0wld5e3Y3CSZqD4RKADGW/WsLkyGyhVcdq66riKIazE7D/258DFRK8wVlcITjsxu6u3gZOfhyVP/z+CjRSVBJhvx+z4B19M0Xnf/V9BauSK50RkrnK5YI9RWrO6mvgB0aqpMVD8vE9V/7DiAGH8zFO1Qxk+w5saPMFtW2EvfEHuZ/9+6JYXkDcAFKbakvXKFQOXya+UjgJuuaIdSGGvJFyZY9ULRkbUC1Qu1XoL/r1tS3OGWFIJbUqxxS4raHt5T55YUQzre000BByBSVAtyUSWMNJuF2aJWqCl/SaBmvtAkxVZftfVNcEuKoJ6CckuKl9yS4uxVEMJuCQByUSXcbTYLz+xJE847YwWqFwiNUty4hp8D2eSWFHd5CfxqveqWFOc7PvdiwAOIENXCRGuuEL/vUwHXUgHXIqGhIk5wS4oFbklhckuKe3wM/GptdUuK97wB+N8AzRQruGekJd4AAAAASUVORK5CYII=" > ' . BConfig::PLUGIN_MENU_NAME, 'bc-menu-cart-woo' ),
            'manage_options',
            BConfig::PLUGIN_SLUG,
            array( $this, 'bc_menu_cart_setting_page_ui')
        );

    }
    public function init_settings()
    {
        // Create option in wp_options.
        if ( false == get_option( self::OPTION_NAME ) ) {
            add_option( self::OPTION_NAME );
        }

        // Register settings.
        register_setting( self::OPTION_NAME, self::OPTION_NAME, array( &$this, 'bc_menu_bar_cart_options_validate' ) );

    }

    /**
     * Build the options page.
     */
    public function bc_menu_cart_setting_page_ui() {
        $all_cart_designs =  BC_Options::get_all_options(self::OPTION_NAME)->posts;
        //check if the option_id is passed in the URL (in case of edit)
        //get that value if available, otherwise, create a new one
        $action = isset($_GET['action']) ? $_GET['action'] : 'edit';
        $option_post_id = isset($_GET['option_id']) ? intval($_GET['option_id']) : 0;

        if (isset($_GET['active_tab']))
        {
            $active_tab = $_GET['active_tab'];

            ?>
            <script>
                //this whole part is just to make the page goes to the right tab after reload. What a fucking pathetic poc
                (function($){
                    $(function(){
                        var active_tab_id = '#' + <?php echo "'" . $active_tab . "'"; ?>;
                        var active_tab_content = $(active_tab_id);
                        var active_tab_li = $('[href='+active_tab_id+']').closest('li');
                        //remove the active class from the si
                        active_tab_li.siblings('li').removeClass('bc-uk-active');
                        active_tab_content.siblings('li').removeClass('bc-uk-active');

                        active_tab_li.addClass('bc-uk-active');
                        active_tab_content.addClass('bc-uk-active');
                    });

                })(jQuery)

            </script>

            <?php }


        //if the action is to trash the page, then delete the option and redirect to a blank page
        if ($action == 'trash' && $option_post_id != 0)
        {
            wp_delete_post($option_post_id);
            exit(wp_redirect(admin_url('admin.php?page=bc_menu_bar_cart&active_tab=create-design-tab')));
        }


        $general_bc_ui = new Options_Form(self::OPTION_NAME, $option_post_id);

        ?>


        <div class="bc-root bc-doc">
            <div class="bc-uk-container">
            <h2><?php _e('Binary Carpenter Menu Cart', 'bc-menu-cart-woo' ) ?></h2>



            <?php
            if (!self::get_menu_array()) {
                ?>
                <div class="bc-uk-alert-danger" style="width:400px; padding:10px;">
                    <h3><?php _e('Important!', 'bc-menu-cart-woo'); ?></h3>
                    <p>
                        <?php _e('You don\'t have any menu yet. Please Go to <strong>Appearance -> Menus</strong> and create a menu to get started.', 'bc-menu-cart-woo'); ?>
                    </p>

                </div>
            <?php } ?>
                <div class="bc-uk-flex" bc-uk-grid>

                    <div class="bc-uk-width-1-1@m">
                        <ul class="bc-uk-tab" data-uk-tab="{connect:'#bc-menu-cart-top-tab'}">
                            <li>
                                <a href="#welcome-tab"><?php _e('Welcome', 'bc-menu-cart-woo'); ?></a>
                            </li>
                            <li>
                                <a href="#create-design-tab"><?php _e('Create new cart design', 'bc-menu-cart-woo'); ?></a>
                            </li>
                            <li>
                                <a href="#link-design-to-menu-tab"><?php _e('Link design to menu', 'bc-menu-cart-woo'); ?></a>
                            </li>

                            <li>
                                <a href="#theme-cart-icon-settings-tab"><?php _e('Theme\'s cart icon settings', 'bc-menu-cart-woo'); ?></a>
                            </li>
                        </ul>

                        <ul class="bc-uk-switcher" id="bc-menu-cart-top-tab">
                            <li class="bc-single-tab" id="welcome-tab">
                                <div>
                                    <h3><?php _e('Thanks for using our plugin', 'bc-menu-cart-woo'); ?></h3>
                                    <p><?php _e('We hope you enjoy it.', 'bc-menu-cart-woo'); ?></p>
                                    <p><?php _e('We made a list of tutorial <a href="https://www.youtube.com/playlist?list=PL6rw2AEN42Eq4M6bJABDJJI5OhrslZ6cM">here on YouTube</a>', 'bc-menu-cart-woo'); ?></p>
                                    <p><?php _e('If you need help, please <a href="https://tickets.binarycarpenter.com/open.php">contact us</a>', 'bc-menu-cart-woo'); ?></p>
                                </div>


                                <?php
                                //try to activate here if it's pro
                                if (!BConfig::IS_FREE)
                                {
                                    $activation_result = json_decode(Activation::activate(BConfig::KEY_CHECK_OPTION));

                                    Static_UI::notice($activation_result->message, $activation_result->status, false, true);
                                } ?>

                                <?php if (BConfig::IS_FREE): ?>
                                    <h3><?php _e('Like this plugin? The PRO version is much more awesome! <a target="_blank" href="https://www.binarycarpenter.com/app/bc-menu-cart-icon-plugin/?src=upgrade-h3-first-page">Check it out</a>', 'bc-menu-cart-woo') ?></h3>
                                <?php endif; ?>


                            </li>

                            <li class="bc-single-tab" id="create-design-tab">
                                <div class="bc-uk-flex" bc-uk-grid>
                                    <div class="bc-uk-width-auto@m">
                                        <a href="<?php echo admin_url('admin.php?page=bc_menu_bar_cart&active_tab=create-design-tab'); ?>" class="bc-uk-button bc-uk-button-primary">Create new design</a>


                                        <?php

                                        Static_UI::heading('Designs you created', 2);

                                        echo '<ul>';
                                        foreach ($all_cart_designs as $design)
                                        {
                                            echo sprintf('<li><a href="%1$s"><i class="icon-edit-01"></i></a> <a href="%2$s"><i class="icon-trash-01"></i></a>%3$s</li>', admin_url('admin.php?page=bc_menu_bar_cart&active_tab=create-design-tab&action=edit&option_id=' . $design->ID), admin_url('admin.php?page=bc_menu_bar_cart&active_tab=create-design-tab&action=trash&option_id=' . $design->ID), $design->post_title);
                                        }

                                        echo '</ul>';


                                        ?>
                                    </div>
                                    <div class="bc-uk-width-2-3@m">
                                        <form class="bc-uk-form">
                                        <ul class="bc-uk-tab" data-uk-tab="{connect:'#bc-menu-cart-settings-tabs'}">
                                            <li>
                                                <a href="#general-tab"><?php _e('General settings', 'bc-menu-cart-woo'); ?></a>
                                            </li>

                                            <li>
                                                <a href="#design-tab"><?php _e('Design settings', 'bc-menu-cart-woo'); ?></a>
                                            </li>
                                        </ul>
                                        <ul class="bc-uk-switcher" id="bc-menu-cart-settings-tabs">

                                            <li id="general-tab">

                                                <?php

                                                if(isset($_GET['option_id']))
                                                {
                                                    $general_bc_ui->card_section(__('Cart icon shortcode', 'bc-menu-cart-woo'), array(
                                                        sprintf('[bc_cart_icon id=%1$s]', intval($_GET['option_id']))
                                                    ));
                                                }


                                                $general_bc_ui->card_section(__('Design title', 'bc-menu-cart-woo'), array(
                                                    $general_bc_ui->input_field(OName::TITLE, false, __('Design title, like post title, easy to remember', 'bc-menu-cart-woo'))

                                                ));


                                                $general_bc_ui->card_section('Cart display', array(
                                                    $general_bc_ui->checkbox(OName::ALWAYS_DISPLAY, false, __('Always show the cart icon on the menu, even when the cart is empty', 'bc-menu-cart-woo')),
                                                    $general_bc_ui->checkbox(OName::DISPLAY_CART_ICON, false, __('Display icon on cart', 'bc-menu-cart-woo')),
//                                                    $general_bc_ui->checkbox(OName::DISPLAY_ITEM_COUNT, false, 'Display item count'),
//                                                    $general_bc_ui->checkbox(OName::DISPLAY_CART_TOTAL, false, 'Display cart total')
                                                ));



                                                $general_bc_ui->card_section(__('Cart layout', 'bc-menu-cart-woo'), array(
                                                    $general_bc_ui->radio(OName::CART_LAYOUT, array(
                                                        '0' => array('content' => plugins_url('../bundle/images/layout_01.jpg', __FILE__), 'disabled' => false),
                                                        '1' => array('content' => plugins_url('../bundle/images/layout_02.jpg', __FILE__), 'disabled' => false),
                                                        '2' => array('content' => plugins_url('../bundle/images/layout_03.jpg', __FILE__), 'disabled' => false),
                                                        '3' => array('content' => plugins_url('../bundle/images/layout_04.jpg', __FILE__), 'disabled' => false),
                                                        '4' => array('content' => plugins_url('../bundle/images/layout_05.jpg', __FILE__), 'disabled' => false),
                                                        '5' => array('content' => plugins_url('../bundle/images/layout_06.jpg', __FILE__), 'disabled' => false),

                                                    ), 'row', 'image', __('Select cart layout on desktop', 'bc-menu-cart-woo'),  array(100, 0)),

                                                    $general_bc_ui->radio(OName::CART_LAYOUT_MOBILE, array(
                                                        '0' => array('content' => plugins_url('../bundle/images/layout_01.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        '1' => array('content' => plugins_url('../bundle/images/layout_02.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        '2' => array('content' => plugins_url('../bundle/images/layout_03.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        '3' => array('content' => plugins_url('../bundle/images/layout_04.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        '4' => array('content' => plugins_url('../bundle/images/layout_05.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        '5' => array('content' => plugins_url('../bundle/images/layout_06.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),

                                                    ), 'row', 'image', __('Select cart layout on mobile', 'bc-menu-cart-woo'),  array(100, 0)),
                                                    $general_bc_ui->input_field(OName::MY_CART_TEXT, 'text', __('"My Cart" replacement', 'bc-menu-cart-woo'), false),
                                                    $general_bc_ui->radio(OName::CART_FLOAT, array(
                                                        'bc-mnc__float-left' => array('content' => 'Left', 'disabled' => false),
                                                        'bc-mnc__float-right' => array('content' => 'Right', 'disabled' => false),
                                                        'bc-mnc__float-none' => array('content' => 'Default', 'disabled' => false),
                                                    ), 'row', 'text', 'Cart float')

                                                ));


                                                $general_bc_ui->setting_fields();

                                                $general_bc_ui->js_post_form();
                                                ?>

                                            </li>

                                            <li id="design-tab">
                                                <?php
                                                $general_bc_ui->card_section(__('Cart icon design', 'bc-menu-cart-woo'), array(



                                                        $general_bc_ui->radio(OName::CART_ICON_TYPE, array(
                                                        'font_icon' => array('content' => __('Font icon', 'bc-menu-cart-woo'), 'disabled' => false),
                                                        'image' => array('content' => __('Image', 'bc-menu-cart-woo'), 'disabled' => BConfig::IS_FREE)

                                                    ), 'row', 'text', __('Cart icon type', 'bc-menu-cart-woo')),

                                                    Static_UI::flex_section(array(
                                                        $general_bc_ui->radio(OName::CART_ICON_FONT, array(
                                                            'icon-cart-01' => array('content' => 'icon-cart-01', 'disabled' => false),
//                                                            'icon-cart-02' => array('content' => 'icon-cart-02', 'disabled' => false),
//                                                            'icon-cart-03' => array('content' => 'icon-cart-03', 'disabled' => false),
                                                            'icon-cart-04' => array('content' => 'icon-cart-04', 'disabled' => false),
                                                            'icon-cart-05' => array('content' => 'icon-cart-05', 'disabled' => false),
                                                            'icon-cart-06' => array('content' => 'icon-cart-06', 'disabled' => false),
                                                            'icon-cart-07' => array('content' => 'icon-cart-07', 'disabled' => false),
                                                            'icon-cart-08' => array('content' => 'icon-cart-08', 'disabled' => false),
                                                            'icon-cart-09' => array('content' => 'icon-cart-09', 'disabled' => false),
                                                            'icon-cart-10' => array('content' => 'icon-cart-10', 'disabled' => false),
                                                            'icon-cart-11' => array('content' => 'icon-cart-11', 'disabled' => false),
                                                            'icon-cart-12' => array('content' => 'icon-cart-12', 'disabled' => false),
                                                            'icon-cart-13' => array('content' => 'icon-cart-13', 'disabled' => false),
                                                            'icon-cart-14' => array('content' => 'icon-cart-14', 'disabled' => false),
                                                            'icon-cart-15' => array('content' => 'icon-cart-15', 'disabled' => false),


                                                        ), 'row', 'icon_font', __('Select icon for your cart', 'bc-menu-cart-woo')),
                                                        $general_bc_ui->image_picker(OName::CART_ICON_IMAGE, __('Select', 'bc-menu-cart-woo'),  __('Select cart image', 'bc-menu-cart-woo'), BConfig::IS_FREE)

                                                    ), 'bc-uk-flex-between'),


                                                        Static_UI::flex_section(array(
                                                            $general_bc_ui->input_field(OName::CART_ICON_WIDTH, 'number', __('Icon width(px)', 'bc-menu-cart-woo'), false, 100),
                                                            $general_bc_ui->input_field(OName::CART_ICON_HEIGHT, 'number', __('Icon height(px)', 'bc-menu-cart-woo'), false, 100),
                                                            $general_bc_ui->input_field(OName::CART_ICON_FONT_SIZE, 'number', __('Icon font size(px)', 'bc-menu-cart-woo'), false, 100),
                                                        ), 'bc-uk-flex-between'),


                                                    BConfig::IS_FREE ? Static_UI::notice(__('Cart icon color  is available in the pro version only. <a href="https://www.binarycarpenter.com/app/bc-menu-cart-icon-plugin/?src=in-plugin-cart-color">Click here to upgrade</a> to unlock all features', 'bc-menu-cart-woo'), 'info', false
                                                    , false): '',
                                                    $general_bc_ui->input_field(OName::CART_ICON_COLOR, 'color', __('Cart icon color (in case you use font icon)', 'bc-menu-cart-woo'), BConfig::IS_FREE, 60)

                                                ));


                                                $general_bc_ui->card_section(__('Cart count circle design', 'bc-menu-cart-woo'),
                                                    array(

                                                        Static_UI::flex_section(array(
                                                            $general_bc_ui->input_field(OName::ITEM_COUNT_CIRCLE_BG_COLOR, 'color', __('Item count circle background color', 'bc-menu-cart-woo'), false, 60),
                                                            $general_bc_ui->input_field(OName::ITEM_COUNT_CIRCLE_TEXT_COLOR, 'color', __('Item count circle text color', 'bc-menu-cart-woo'), false, 60),
                                                        ), 'bc-uk-flex-between'),
                                                        Static_UI::flex_section(array(
                                                            $general_bc_ui->input_field(OName::ITEM_COUNT_CIRCLE_WIDTH, 'number', __('Item count circle width', 'bc-menu-cart-woo'), false, 100),
                                                            $general_bc_ui->input_field(OName::ITEM_COUNT_CIRCLE_HEIGHT, 'number', __('Item count circle height', 'bc-menu-cart-woo'), false, 100),
                                                            $general_bc_ui->input_field(OName::ITEM_COUNT_CIRCLE_FONT_SIZE, 'number', __('Item count font size', 'bc-menu-cart-woo'), false, 100),
                                                        ), 'bc-uk-flex-between')
                                                    )

                                                    );

                                                if (BConfig::IS_FREE)
                                                {
                                                    Static_UI::notice(__('The section below is available in the pro version only. <a href="https://www.binarycarpenter.com/app/bc-menu-cart-icon-plugin/?src=in-plugin">Click here to upgrade</a> to unlock all features', 'bc-menu-cart-woo'), 'info');
                                                }

                                                $general_bc_ui->card_section(__('On cart item action behavior', 'bc-menu-cart-woo'), array(
                                                        Static_UI::flex_section(array(
                                                            $general_bc_ui->select(OName::ON_CART_ICON_CLICK, array(
                                                                'go_to_cart' => __('Go to cart', 'bc-menu-cart-woo'),
                                                                'do_nothing' => __('Do nothing', 'bc-menu-cart-woo'),
                                                                'show_cart_list' => __('Show cart items list', 'bc-menu-cart-woo')

                                                            ), __('On cart click', 'bc-menu-cart-woo'), BConfig::IS_FREE, false),
                                                            $general_bc_ui->select(OName::ON_CART_ICON_HOVER, array(
                                                                'do_nothing' => __('Do nothing', 'bc-menu-cart-woo'),
                                                                'show_cart_list' => __('Show cart items list', 'bc-menu-cart-woo')
                                                            ), __('On cart hover', 'bc-menu-cart-woo'), BConfig::IS_FREE, false)
                                                        ), 'bc-uk-flex-between'),


                                                ));



/**

This section adds style to the cart details. 
One style needs to have three things:
    1. define a value here
    2. define the style in cart-list-design.scss in src/css
    3. define the style's skeleton in BC_Menu_Cart_Display.php

    Without any of the three, new style does not work.
*/
                                                $general_bc_ui->card_section(__('Cart items list design', 'bc-menu-cart-woo'), array(
                                                    $general_bc_ui->radio(OName::CART_LIST_STYLE_CLASS, array(
                                                        'bc-mnc__cart-details-style-1' => array('content' => plugins_url('../bundle/images/cart-details-design-01.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        'bc-mnc__cart-details-style-2' => array('content' => plugins_url('../bundle/images/cart-details-design-02.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        'bc-mnc__cart-details-style-3' => array('content' => plugins_url('../bundle/images/cart-details-design-03.jpg', __FILE__), 'disabled' => BConfig::IS_FREE),
                                                        'bc-mnc__cart-details-style-4' => array('content' => plugins_url('../bundle/images/cart-details-design-04.gif', __FILE__), 'disabled' => BConfig::IS_FREE),



                                                    ), 'row', 'image', __('Select preset designs for the cart list', 'bc-menu-cart-woo'), array(70, 0)),



/**
 * @TODO later version
 */
//                                                    $general_bc_ui->select(OName::CART_LIST_DISPLAY_STYLE, array(
//                                                        'drop_down' => 'Drop down',
//                                                        'slide_from_right' => 'Slide from right',
//                                                        'slide_from_left' => 'Slide from left',
//                                                        'full_page' => 'Full page'
//                                                    ), 'Cart list display style', false, false)

                                                ));


                                                $general_bc_ui->card_section(__('Cart list close button styling', 'bc-menu-cart-woo'), array(
                                                    $general_bc_ui->radio(OName::CLOSE_CART_LIST_ICON, array(
                                                        'icon-close-01' => array('content' => 'icon-close-01', 'disabled' => BConfig::IS_FREE),

                                                        'icon-close-03' => array('content' => 'icon-close-03', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-04' => array('content' => 'icon-close-04', 'disabled' => BConfig::IS_FREE),

                                                        'icon-close-06' => array('content' => 'icon-close-06', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-07' => array('content' => 'icon-close-07', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-08' => array('content' => 'icon-close-08', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-09' => array('content' => 'icon-close-09', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-10' => array('content' => 'icon-close-10', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-11' => array('content' => 'icon-close-11', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-12' => array('content' => 'icon-close-12', 'disabled' => BConfig::IS_FREE),

                                                    ), 'row', 'icon_font', __('Select close cart list icon (display at the top right of the cart list)', 'bc-menu-cart-woo')),
                                                    $general_bc_ui->input_field(OName::CLOSE_CART_LIST_ICON_FONT_SIZE, 'number', __('Icon\'s font size', 'bc-menu-cart-woo'), BConfig::IS_FREE),
                                                ));

                                                $general_bc_ui->card_section(__('Product image styling', 'bc-menu-cart-woo'), array(
                                                    $general_bc_ui->checkbox(OName::DISPLAY_PRODUCT_IMAGE, BConfig::IS_FREE, __('Display product image', 'bc-menu-cart-woo')),
                                                    Static_UI::flex_section(array(
                                                        $general_bc_ui->input_field(OName::PRODUCT_IMAGE_WIDTH, 'number', __('Product image width', 'bc-menu-cart-woo'), BConfig::IS_FREE),
                                                        $general_bc_ui->input_field(OName::PRODUCT_IMAGE_HEIGHT, 'number', __('Product image height', 'bc-menu-cart-woo'), BConfig::IS_FREE),

                                                    ), 'bc-uk-flex-between')
                                                ));

                                                $general_bc_ui->card_section(__('Product remove button styling', 'bc-menu-cart-woo'), array(

                                                    $general_bc_ui->radio(OName::REMOVE_PRODUCT_ICON, array(
                                                        'icon-close-01' => array('content' => 'icon-close-01', 'disabled' => BConfig::IS_FREE),

                                                        'icon-close-03' => array('content' => 'icon-close-03', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-04' => array('content' => 'icon-close-04', 'disabled' => BConfig::IS_FREE),

                                                        'icon-close-06' => array('content' => 'icon-close-06', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-07' => array('content' => 'icon-close-07', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-08' => array('content' => 'icon-close-08', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-09' => array('content' => 'icon-close-09', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-10' => array('content' => 'icon-close-10', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-11' => array('content' => 'icon-close-11', 'disabled' => BConfig::IS_FREE),
                                                        'icon-close-12' => array('content' => 'icon-close-12', 'disabled' => BConfig::IS_FREE),

                                                    ), 'row', 'icon_font', __('Select remove product icon (display in front of every products in cart)', 'bc-menu-cart-woo')),


                                                    Static_UI::flex_section(array(
                                                        $general_bc_ui->input_field(OName::PRODUCT_REMOVE_BUTTON_FONT_SIZE, 'number', __('Product removal icon font size', 'bc-menu-cart-woo'), BConfig::IS_FREE),
                                                    )),
                                                ));


                                                $general_bc_ui->card_section(__('Product quantity change styling', 'bc-menu-cart-woo'), array(
                                                        $general_bc_ui->checkbox(OName::DISPLAY_QUANTITY_CHANGE_BOX, BConfig::IS_FREE, __('Display quantity change option for every product in cart', 'bc-menu-cart-woo')),
                                                    Static_UI::flex_section(array(
                                                                $general_bc_ui->radio(OName::QUANTITY_CHANGE_BOX_INCREASE_ICON, array(
                                                                    'icon-up-01' => array('content' => 'icon-up-01', 'disabled' => false),
                                                                    'icon-plus-04' => array('content' => 'icon-plus-04', 'disabled' => BConfig::IS_FREE),
                                                                    'icon-plus-02' => array('content' => 'icon-plus-02', 'disabled' => BConfig::IS_FREE),
                                                                    'icon-plus-01' => array('content' => 'icon-plus-01', 'disabled' => BConfig::IS_FREE),

                                                                    'icon-right-01' => array('content' => 'icon-right-01', 'disabled' => BConfig::IS_FREE),
                                                                    'icon-right-02' => array('content' => 'icon-right-02', 'disabled' => BConfig::IS_FREE),

                                                                ) , 'row', 'icon_font', __('Icon for increase button', 'bc-menu-cart-woo')),

                                                            $general_bc_ui->radio(OName::QUANTITY_CHANGE_BOX_DECREASE_ICON, array(
                                                                'icon-down-01' => array('content' => 'icon-down-01', 'disabled' => false),
                                                                'icon-minus-01' => array('content' => 'icon-minus-01', 'disabled' => BConfig::IS_FREE),
                                                                'icon-minus-02' => array('content' => 'icon-minus-02', 'disabled' => BConfig::IS_FREE),
                                                                'icon-minus-03' => array('content' => 'icon-minus-03', 'disabled' => BConfig::IS_FREE),

                                                                'icon-left-03' => array('content' => 'icon-left-03', 'disabled' => BConfig::IS_FREE),
                                                                'icon-left-01' => array('content' => 'icon-left-01', 'disabled' => BConfig::IS_FREE),

                                                            ) , 'row', 'icon_font', __('Icon for increase button', 'bc-menu-cart-woo'))

                                                        ), 'bc-uk-flex-between')
                                                ));


                                                $general_bc_ui->card_section(__('Move the cart icon on mobile', 'bc-menu-cart-woo'), array(
                                                    Static_UI::flex_section(array(

                                                        $general_bc_ui->radio(OName::DESIGN_MOBILE_POSITION, array(
                                                            'left' => array('content' => __('Left', 'bc-menu-cart-woo'), 'disabled' => false),
                                                            'right' => array('content' => __('Right', 'bc-menu-cart-woo'), 'disabled' => false),

                                                        ), 'row', 'text', __('Position on mobile', 'bc-menu-cart-woo')),
                                                        $general_bc_ui->input_field(OName::DESIGN_MOBILE_POSITION_RELATIVE_TO_ELEMENT, 'text',   __('Element selector', 'bc-menu-cart-woo'), BConfig::IS_FREE),
                                                        $general_bc_ui->checkbox(OName::DESIGN_MOBILE_REMOVE_ORIGIN_ELEMENT, BConfig::IS_FREE,   __('Remove from original'), 'bc-menu-cart-woo'),



                                                    ), 'bc-uk-flex-between'),

                                                    $general_bc_ui->textarea(OName::DESIGN_MOBILE_EXTRA_STYLE, __('enter extra CSS rules here (property: value;), if any', 'bc-menu-cart-woo'), BConfig::IS_FREE)
                                                ));

                                                $general_bc_ui->card_section(__('Other cart list settings', 'bc-menu-cart-woo'), array(
                                                        Static_UI::flex_section(array(
                                                            $general_bc_ui->input_field(OName::CART_LIST_HEADER_TEXT, 'text', __('Cart list header text (currently is "Your cart")', 'bc-menu-cart-woo'), BConfig::IS_FREE),
                                                            $general_bc_ui->input_field(OName::CART_LIST_SUBTOTAL_TEXT, 'text', __('Cart list subtotal text (default is "Subtotal")', 'bc-menu-cart-woo'), BConfig::IS_FREE),
                                                        ), 'bc-uk-flex-between'),


                                                    $general_bc_ui->checkbox(OName::DISPLAY_GO_TO_CART_BUTTON, BConfig::IS_FREE, __('Display go to cart button', 'bc-menu-cart-woo')),
                                                    Static_UI::flex_section(array(

                                                        $general_bc_ui->input_field(OName::GO_TO_CART_BUTTON_BG_COLOR, 'color',  __('Go to cart button background color', 'bc-menu-cart-woo'), BConfig::IS_FREE, 60),
                                                        $general_bc_ui->input_field(OName::GO_TO_CART_BUTTON_TEXT, 'text',  __('Go to cart button text', 'bc-menu-cart-woo'), BConfig::IS_FREE),

                                                    ), 'bc-uk-flex-between'),


                                                    $general_bc_ui->checkbox(OName::DISPLAY_GO_TO_CHECKOUT_BUTTON, BConfig::IS_FREE, __('Display go to checkout button'), 'bc-menu-cart-woo'),
                                                    Static_UI::flex_section(array(

                                                        $general_bc_ui->input_field(OName::GO_TO_CHECKOUT_BUTTON_BG_COLOR, 'color', __('Go to checkout button background color', 'bc-menu-cart-woo'), BConfig::IS_FREE, 60),
                                                        $general_bc_ui->input_field(OName::GO_TO_CHECKOUT_BUTTON_TEXT, 'text',  __('Go to cart button text', 'bc-menu-cart-woo'), BConfig::IS_FREE),
                                                    ), 'bc-uk-flex-between')
                                                ));




                                    //output a hidden field to instruct the script to reload or redirect the page or not
                                                //if the option_id get parameter is not set, that means the user
                                                //is creating the option, not editing it, thus, it is necessary to redirect
                                                //after save
                                                if (!isset($_GET['option_id']))
                                                {
                                                    $general_bc_ui->raw_hidden(OName::REDIRECT_URL, admin_url('admin.php?page=bc_menu_bar_cart&action=edit&option_id=' . $general_bc_ui->get_option_post_id()));
                                                }




                                                ?>

                                            </li>

                                        </ul>
                                        <?php $general_bc_ui->submit_button(__('Save settings', 'bc-menu-cart-woo')); ?>
                                    </form>
                                    </div>
                                </div>
                            </li>

                            <li class="bc-single-tab" id="link-design-to-menu-tab">

                                <h3>Link menu to design</h3>
                                <p>Please select the design for your menus</p>
                                <form class="bc-uk-form">
                                    <?php
                                    $link_menu_options = BC_Options::get_all_options('bc_menu_cart_linked_menu');



                                    if (count($all_cart_designs) == 0)
                                    {
                                        echo __('You have not created any design. Go create one', 'bc-menu-cart-woo');
                                        return;
                                    }

                                    $design_radios = array();

                                    foreach ($all_cart_designs as $design)
                                    {
                                        $design_radios[$design->ID] = array(
                                                'content' => $design->post_title,
                                                'disabled' => false
                                        );
                                    }


                                    if ($link_menu_options->have_posts())
                                        $link_menu_ui = new Options_Form('bc_menu_cart_linked_menu', $link_menu_options->next_post()->ID);
                                    else
                                        $link_menu_ui = new Options_Form('bc_menu_cart_linked_menu', 0);




                                    foreach (self::get_menu_array() as $menu)
                                    {
                                        echo $link_menu_ui->radio($menu['slug'], $design_radios, 'row', 'text', $menu['name']);
                                    }

                                    ?>
<!--                                    <h3>Position on mobile</h3> -->

                                    <?php



                                    $link_menu_ui->setting_fields();
                                    $link_menu_ui->js_post_form();
                                    $link_menu_ui->submit_button(__('Save links', 'bc-menu-cart-woo'));

                                    ?>


                                </form>
                            </li>

                            <li class="bc-single-tab" id="theme-cart-icon-settings-tab">
                                <form class="bc-uk-form">
                                    <?php
                                    $theme_cart_option = BC_Options::get_all_options('bc_menu_cart_theme_cart_icon');


                                    if ($theme_cart_option->have_posts())
                                        $theme_cart_ui = new Options_Form('bc_menu_cart_theme_cart_icon', $theme_cart_option->next_post()->ID);
                                    else
                                        $theme_cart_ui = new Options_Form('bc_menu_cart_theme_cart_icon', 0);


                                    $theme_cart_ui->card_section(__('Theme\'s cart icon settings', 'bc-menu-cart-woo'), array(
                                        $theme_cart_ui->checkbox(OName::HIDE_THEME_CART, false, __('Hide theme cart icon?', 'bc-menu-cart-woo')),
                                        $theme_cart_ui->input_field(OName::THEME_CART_CSS_SELECTOR, 'text', __('Theme\'s cart CSS selector', 'bc-menu-cart-woo'), false)
                                    ));



                                    $theme_cart_ui->setting_fields();
                                    $theme_cart_ui->js_post_form();
                                    $theme_cart_ui->submit_button(__('Save settings', 'bc-menu-cart-woo'));

                                    ?>

                                </form>


                            </li>


                        </ul>

                    </div>
                </div>



            </div> <!-- End container -->
        </div>
        <?php
    }

    /**
     * Get menu array.
     *
     * @return array menu slug => menu name
     */
    public static function get_menu_array() {
        $menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) );
        $menu_list = array();

        foreach ( $menus as $menu ) {
            $menu_list[] = array(
                    'slug' => $menu->slug,
                    'name' => $menu->name);
        }

        if (!empty($menu_list))
            return $menu_list;

        return array();
    }

    /**
     * Get array of active shop plugins
     *
     * @return array plugin slug => plugin name
     */
    public function get_shop_plugins() {
        $active_shop_plugins = BC_Menu_Cart_Display::get_active_shops();

        //switch keys & values, then strip plugin path to folder
        foreach ($active_shop_plugins as $key => $value) {
            $filtered_active_shop_plugins[dirname($value)] = $key;
        }

        $active_shop_plugins = isset($filtered_active_shop_plugins) ? $filtered_active_shop_plugins:'';

        return $active_shop_plugins;
    }


    /**
     * Validate/sanitize options input
     */
    public function bc_menu_bar_cart_options_validate( $input ) {
        // Create our array for storing the validated options.
        $output = array();

        // Loop through each of the incoming options.
        foreach ( $input as $key => $value ) {

            // Check to see if the current option has a value. If so, process it.
            if ( isset( $input[$key] ) ) {
                // Strip all HTML and PHP tags and properly handle quoted strings.
                if ( is_array( $input[$key] ) ) {
                    foreach ( $input[$key] as $sub_key => $sub_value ) {
                        $output[$key][$sub_key] = strip_tags( stripslashes( $input[$key][$sub_key] ) );
                    }

                } else {
                    $output[$key] = strip_tags( stripslashes( $input[$key] ) );
                }
            }
        }

        // Return the array processing any additional functions filtered by this action.
        return apply_filters( 'bc_menu_bar_cart_validate_input', $output, $input );
    }


}
