<?php
if ( ! class_exists('BC_Menu_Cart_Woo_Helper') ) {
	class BC_Menu_Cart_Woo_Helper {

		/**
		 * Construct.
		 */
		public function __construct() {
		}
	
		public function menu_item() {
			global $woocommerce;

			// make sure cart is loaded! https://wordpress.org/support/topic/activation-breaks-customise?replies=10#post-7908988
			if (empty($woocommerce->cart)) {
				$woocommerce->cart = new WC_Cart();
			}

			$menu_item = array(
				'cart_url'				=> $this->cart_url(),
				'shop_page_url'			=> $this->shop_url(),
				'cart_contents_count'	=> $woocommerce->cart->get_cart_contents_count(),
				'cart_total'			=> $this->get_cart_total(),
			);
		
			return $menu_item;
		}

		public function get_cart_total()
        {
            return 'need totla';
        }
		public function cart_url() {
			if ( defined('WOOCOMMERCE_VERSION') && version_compare( WOOCOMMERCE_VERSION, '2.5.2', '>=' ) ) {
				return wc_get_cart_url();
			} else {
				$cart_page_id = wc_get_page_id('cart');
				if ( $cart_page_id ) {
					return apply_filters( 'woocommerce_get_cart_url', get_permalink( $cart_page_id ) );
				} else {
					return '';
				}
			}
		}

		public function shop_url() {
			if ( defined('WOOCOMMERCE_VERSION') && version_compare( WOOCOMMERCE_VERSION, '2.5.2', '>=' ) ) {
				return wc_get_page_permalink( 'shop' );
			} else {
				return get_permalink( wc_get_page_id( 'shop' ) );
			}
		}
	}
}