<?php
/**
 * The sole purpose of this class is to generate the cart details list
 * Available in pro version only
 */

namespace BinaryCarpenter\BC_MNC;


use BinaryCarpenter\BC_MNC\Options_Name as Oname;
use WC_Product;
use WC_Product_Variable;

class Cart_Details
{

    /**
     * The only public function that outer class can call. This generate the cart items list (the sub menu appears
     * when visitors click on or hover on the cart icon)
     * @param BC_Options $design_options
     * @return string
     */
    public static function generate_cart_items_list(BC_Options $design_options)
    {
        
        $cart_list_style_class = $design_options->get_string(Oname::CART_LIST_STYLE_CLASS, 'bc-mnc__cart-details-style-1');
        return self::generate_cart_list_html_by_style_class($cart_list_style_class, $design_options);
    }

    /**
     * @param BC_Options $design_options
     * @return string
     */
    private static function generate_cart_close_icon_html(BC_Options $design_options)
    {
        $cart_list_close_icon = $design_options->get_string(Oname::CLOSE_CART_LIST_ICON, 'icon-close-01', false);
        $cart_list_close_icon_font_size = $design_options->get_int(Oname::CLOSE_CART_LIST_ICON_FONT_SIZE, 20, false) . 'px';
        return sprintf('<i style="font-size: %1$s;" class="bc-mnc__cart-details--close-button %2$s" title="%3$s"></i>', $cart_list_close_icon_font_size, $cart_list_close_icon, __('Close', 'bc-menu-cart-woo'));
    }


    /**
     * Return the HTML content for the product image
     * @param BC_Options $design_options
     * @param WC_Product $product
     * @param $variation_id
     * @return string
     */
    private static function generate_product_image_html(BC_Options $design_options, WC_Product $product, $variation_id)
    {
        if (!$design_options->get_bool(Oname::DISPLAY_PRODUCT_IMAGE, true))
            return '';

        $image_width = $design_options->get_int(Oname::PRODUCT_IMAGE_WIDTH) > 0 ? sprintf('width: %1$s;' ,$design_options->get_int(Oname::PRODUCT_IMAGE_WIDTH) .'px') : '';
        $image_height = $design_options->get_int(Oname::PRODUCT_IMAGE_HEIGHT) > 0 ? sprintf('width: %1$s;' ,$design_options->get_int(Oname::PRODUCT_IMAGE_HEIGHT) .'px') : '';
        $image_style = sprintf('style="%1$s %2$s"', $image_width, $image_height);

        if ($variation_id == 0)
        {
            $product_image = $product->get_image();

            return sprintf('<div %1$s class="bc-mnc__cart-details--single-item__product-image">%2$s</div>', $image_style, $product_image);
        } else
        {
            $variations = (new WC_Product_Variable($product->get_id()))->get_available_variations();

            if (count($variations) > 0)
            {
                foreach ($variations as $v)
                {
                    if (isset($v['variation_id']) && ($v['variation_id']  == $variation_id) )
                    {
                        return sprintf('<div %1$s class="bc-mnc__cart-details--single-item__product-image">%2$s</div>', $image_style, wp_get_attachment_image($v['image_id'], 'woocommerce_thumbnail'));

                    }

                }

            }

        }

    }

    private static function generate_product_price_html(\WC_Product $product, $variation_id = 0)
    {
        if ($variation_id == 0)
            return wc_price($product->get_price());

        $variations = (new WC_Product_Variable($product->get_id()))->get_available_variations();

        if (count($variations) > 0)
        {
            foreach ($variations as $v)
            {
                if (isset($v['variation_id']) && ($v['variation_id']  == $variation_id) )
                {
                    return wc_price($v['display_price']);
                }

            }

        }


        return $product->get_price();



    }

    /**
     *
     *
     * @param BC_Options $design_options
     * @param array $product_item a single item in cart, this is a WooCommerce cart array returned by calling WC()->cart->get_cart()
     * @return string
     */
    private static function generate_product_removal_button_html(BC_Options $design_options, $product_item)
    {

        $icon_close = $design_options->get_string(Oname::REMOVE_PRODUCT_ICON, 'icon-close-01');

        $product_removal_font_size = $design_options->get_int(Oname::PRODUCT_REMOVE_BUTTON_FONT_SIZE, 16, false) . 'px';

        //add ajax_add_to_cart to the remove button to display the loading icon
        return sprintf('<div style="font-size: %1$s;" class="bc-mnc__cart-details--remove-product-icon %2$s" title="%3$s" ></div>', $product_removal_font_size, $icon_close, __('Remove this product from cart', 'bc-menu-cart-woo') );
    }


    /**
     * Generate cart list HTML based ont the style class,
     * Some style classes may share the HTML structure
     *
     * @param $cart_list_style_class
     * @param BC_Options $design_options
     * @return string
     */
    private static function generate_cart_list_html_by_style_class($cart_list_style_class, BC_Options $design_options)
    {

        $display_go_to_cart = $design_options->get_bool(Oname::DISPLAY_GO_TO_CART_BUTTON, true);
        $display_go_to_checkout = $design_options->get_bool(Oname::DISPLAY_GO_TO_CHECKOUT_BUTTON, true);

        $go_to_cart_html = $display_go_to_cart ? self::generate_go_to_cart_button($design_options) : '';
        $go_to_checkout_html = $display_go_to_checkout ? self::generate_go_to_checkout_button($design_options) : '';


        $cart_total_html = sprintf('<div class="bc-uk-flex bc-uk-flex-between bc-uk-width-1-1 bc-mnc__cart-details--cart-total"><div class="bc-mnc__cart-details--cart-total__title"><strong>%1$s </strong></div><div class="bc-mnc__cart-details--cart-total__amount">%2$s</div></div>', $design_options->get_string(Oname::CART_LIST_SUBTOTAL_TEXT, __('Subtotal', 'bc-menu-cart-woo'), false), Cart_Display::generate_cart_total());

        $mini_cart_heading = sprintf('<h3 class="bc-mnc__cart-details--header">%1$s</h3>', $design_options->get_string(Oname::CART_LIST_HEADER_TEXT, __('Your cart', 'bc-menu-cart-woo')));



        //style 1 and 2 share the same skeleton
        if ($cart_list_style_class == 'bc-mnc__cart-details-style-1' || $cart_list_style_class == 'bc-mnc__cart-details-style-2' )
        {
            $cart_items_html = '';
            //the list of items in cart
            $cart_items = WC()->cart->get_cart();
            $cart_checkout_buttons = sprintf('<div class="bc-mnc__cart-checkout-container bc-uk-flex bc-uk-flex-between">%1$s %2$s</div>', $go_to_cart_html, $go_to_checkout_html);

            foreach ($cart_items as $single_cart_item)
            {
                $product_id = $single_cart_item['product_id'];
                $variation_id = $single_cart_item['variation_id'];
                $quantity = $single_cart_item['quantity'];
                $variation_array = $single_cart_item['variation'];
                $line_sub_total = $single_cart_item['line_subtotal'];
                $product_cart_data = self::generate_single_item_cart_data($single_cart_item);

                $variations_string = '';

                if (count($variation_array) > 0)
                {
                    foreach($variation_array as $v)
                        $variations_string .= sprintf('<span class="bc-mnc__single-variation">%1$s</span>', wc_attribute_label($v));
                }

                $product =  new WC_Product($product_id);

                //Get the product removal button HTML
                $remove_product_button_html = self::generate_product_removal_button_html($design_options, $single_cart_item);
                $product_title = self::generate_product_title_html($product, $design_options);
                $product_price = self::generate_product_price_html($product, $variation_id);



                //total string of a single product, it is style dependant
                $total_string = sprintf('<span>%1$s</span> x <span>%2$s</span> = <strong>%3$s</strong>', $product_price, $quantity, wc_price($line_sub_total));

                $product_image_html = self::generate_product_image_html($design_options, $product, $variation_id);

                $product_quantity_change = self::generate_product_change_section_html($design_options, $quantity);

                $cart_items_html .= sprintf('<div class="bc-mnc__cart-details--single-item"><div class="bc-uk-flex"> %5$s %1$s  <div class="bc-mnc__cart-details--single-item__info bc-uk-width-1-1"> <div class="bc-mnc__cart-details--single-item__info--title">%2$s</div> <div class="bc-mnc__cart-details--single-item__info--attributes"> %3$s<!-- display variation name here, if not blank --> </div> <div class="bc-mnc__cart-details--single-item__info--order-total"> %4$s<!-- display qty x price = total here --> </div> %6$s <!-- display the product change boxes here -->  </div>  </div>%7$s</div>', $product_image_html, $product_title, $variations_string, $total_string, $remove_product_button_html, $product_quantity_change, $product_cart_data);


            }

            //wrap the $$cart_items_html inside a div
            $cart_items_html = '<div class="bc-mnc__cart-details--all-items">' . $cart_items_html . '</div>';

            return sprintf('<section class="bc-mnc__cart-details--items-section bc-menu-cart-cart-details-%1$s clearfix">  %2$s %3$s %4$s %5$s %6$s <div></div></section>', $design_options->get_post_id(), $mini_cart_heading, $cart_items_html,$cart_total_html,  $cart_checkout_buttons, self::generate_cart_close_icon_html($design_options));
        } else if ($cart_list_style_class == 'bc-mnc__cart-details-style-3' || $cart_list_style_class == 'bc-mnc__cart-details-style-4')
        {
            $cart_items_html = '';
            //the list of items in cart
            $cart_items = WC()->cart->get_cart();
            $cart_checkout_buttons = sprintf('<div class="bc-mnc__cart-checkout-container bc-uk-flex bc-uk-flex-between">%1$s %2$s</div>', $go_to_cart_html, $go_to_checkout_html);

            foreach ($cart_items as $single_cart_item)
            {
                $product_id = $single_cart_item['product_id'];
                $variation_id = $single_cart_item['variation_id'];
                $quantity = $single_cart_item['quantity'];
                $variation_array = $single_cart_item['variation'];
                $line_sub_total = $single_cart_item['line_subtotal'];
                $product_cart_data = self::generate_single_item_cart_data($single_cart_item);

                $variations_string = '';

                if (count($variation_array) > 0)
                {
                    foreach($variation_array as $v)
                        $variations_string .= sprintf('<span class="bc-mnc__single-variation">%1$s</span>', wc_attribute_label($v));
                }

                $product =  new WC_Product($product_id);

                //Get the product removal button HTML
                $remove_product_button_html = self::generate_product_removal_button_html($design_options, $single_cart_item);
                $product_title = self::generate_product_title_html($product, $design_options);


                //total string of a single product, it is style dependant
                $total_string = sprintf('<strong>%1$s</strong>', wc_price($line_sub_total));

                $product_image_html = self::generate_product_image_html($design_options, $product, $variation_id);

                $product_quantity_change = self::generate_product_change_section_html($design_options, $quantity, 'increase');

                $cart_items_html .= sprintf('<div class="bc-mnc__cart-details--single-item"><div class="bc-uk-flex"> %5$s %1$s  <div class="bc-mnc__cart-details--single-item__info bc-uk-width-1-1"> <div class="bc-mnc__cart-details--single-item__info--title">%2$s</div> <div class="bc-mnc__cart-details--single-item__info--attributes"> %3$s<!-- display variation name here, if not blank --> </div> <div class="bc-mnc__cart-details--single-item__info--order-total"> %4$s<!-- display qty x price = total here --> </div>   </div>  </div>%6$s <!-- display the product change boxes here --> %7$s</div>', $product_image_html, $product_title, $variations_string, $total_string, $remove_product_button_html, $product_quantity_change, $product_cart_data);


            }

            //wrap the $$cart_items_html inside a div
            $cart_items_html = '<div class="bc-mnc__cart-details--all-items">' . $cart_items_html . '</div>';
            return sprintf('<section class="bc-mnc__cart-details--items-section bc-menu-cart-cart-details-%1$s clearfix">  %2$s %3$s %4$s %5$s %6$s <div></div></section>', $design_options->get_post_id(), $mini_cart_heading, $cart_items_html,$cart_total_html,  $cart_checkout_buttons, self::generate_cart_close_icon_html($design_options));
        }


        return '';

    }


    /**
     * Generate a hidden div/span to store the cart data (cart key, item id...) to update product's
     * quantity via ajax
     * @param array $product_item details about a single item in cart, WC()->cart->get_cart()
     * @return string
     */
    private static function generate_single_item_cart_data($product_item)
    {
        $product_id = $product_item['product_id'];

        $variation_id = $product_item['variation_id'];
        $quantity = $product_item['quantity'];
        $cart_key = $product_item['key'];
        return sprintf('<span style="display: none;" class="bc-mnc__cart-details--single-item__cart-info" data-product-id="%1$s" data-variation-id="%2$s" data-product-quantity="%3$s" data-key="%4$s"></span>', $product_id, $variation_id, $quantity, $cart_key);
    }


    private static function generate_product_change_section_html(BC_Options $design_options, $initial_quantity, $left_icon = 'decrease')
    {
        $display_section = $design_options->get_bool(Oname::DISPLAY_QUANTITY_CHANGE_BOX, false);

        if (!$display_section)
            return '';
        $icon_color = $design_options->get_string(Oname::QUANTITY_CHANGE_BOX_ICON_COLOR, '#222');
        $increase_icon = $design_options->get_string(Oname::QUANTITY_CHANGE_BOX_INCREASE_ICON, 'icon-save-01',false);
        $decrease_icon = $design_options->get_string(Oname::QUANTITY_CHANGE_BOX_DECREASE_ICON, 'icon-close-01',false);

        $style = sprintf('style="color: %1$s !important"', $icon_color);

        return sprintf('<div class="bc-mnc__cart-details--single-item__product-quantity"><i class="bc-mnc__cart-details--single-item__product-quantity--decrease %1$s" %6$s title="%4$s"></i> <span class="bc-mnc__cart-details--single-item__product-quantity--count">%2$s</span>  <i class="bc-mnc__cart-details--single-item__product-quantity--increase %3$s" %6$s title="%5$s"></i></div>', $decrease_icon, $initial_quantity, $increase_icon, __('Decrease one item', 'bc-menu-cart-woo'),  __('Increase one item', 'bc-menu-cart-woo'), $style);
    }


    private static function generate_product_title_html(WC_Product $product, BC_Options $design_options)
    {
    	$title_color = $design_options->get_string(Oname::SINGLE_ITEM_TITLE_COLOR, '#222222');
    	$title_size = $design_options->get_int(Oname::SINGLE_ITEM_TITLE_FONT_SIZE, 16);

    	$style = sprintf('style = "font-size: %1$s; color: %2$s;"', $title_size . 'px', $title_color);

        return sprintf('<a %3$s class="bc-mnc__product-title" href="%1$s">%2$s</a>',$product->get_permalink(), $product->get_title(), $style);
    }

    private static function generate_go_to_cart_button(BC_Options $design_options)
    {

        $bg_color = $design_options->get_string(Oname::GO_TO_CART_BUTTON_BG_COLOR, '#ffb663');
        $text = $design_options->get_string(Oname::GO_TO_CART_BUTTON_TEXT, __('View cart', 'bc-menu-cart-woo') , false);

        return sprintf('<div class="bc-mnc__cart-details--to-cart-button " ><a style="background-color: %1$s;" href="%2$s">%3$s</a></div>', $bg_color, wc_get_cart_url(), $text);
    }

    private static function generate_go_to_checkout_button(BC_Options $design_options)
    {
        $bg_color = $design_options->get_string(Oname::GO_TO_CHECKOUT_BUTTON_BG_COLOR, '#ffb663');
        $text = $design_options->get_string(Oname::GO_TO_CHECKOUT_BUTTON_TEXT, __('Checkout', 'bc-menu-cart-woo'), false );

        return sprintf('<div class="bc-mnc__cart-details--to-checkout-button "><a  style="background-color: %1$s;"  href="%2$s">%3$s</a></div>', $bg_color, wc_get_checkout_url(), $text);
    }


}
