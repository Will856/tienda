<?php
/**
 * Created by PhpStorm.
 * User: MYN
 * Date: 5/9/2019
 * Time: 8:57 AM
 */
namespace BinaryCarpenter\BC_MNC;
class BC_Menu_Cart_Config
{
    const PLUGIN_NAME = 'BC Menu Bar Cart';
    const PLUGIN_MENU_NAME = 'BC Menu Cart';
    const PLUGIN_SLUG = 'bc_menu_bar_cart';
    const PLUGIN_TEXT_DOMAIN = 'bc-menu-cart-woo';
    const PLUGIN_OPTION_NAME = 'bc_menu_bar_cart_option_name';
    const PLUGIN_COMMON_HANDLER = 'bc_menu_bar_cart_handler';
    const IS_FREE = true;//is free or pro
    const KEY_CHECK_OPTION = 'bc_menu_cart_key_check';
}
