<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cliente' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lt;(Vs/?CMZkn{+#r:I-=V*Lf:H8F,PFyY%WR`W~iN>m9K~;(HNy{9Um,B|?6C5$' );
define( 'SECURE_AUTH_KEY',  'Urx({:mf75?f`l:Ty3)g&S_-}UbT7W?UOC#S=ce<;4k>I|J_W`g*/B3t]g;On+_K' );
define( 'LOGGED_IN_KEY',    'liZ9`mEE,V:8NzEhc8CoMgP^ $=/#(.;]F, @pvY8ik$^(]5*y?dr!MDUn;}uM{m' );
define( 'NONCE_KEY',        'l]G^~}3wCz4ByibVOTHR[tfFP=uib`XEoty2wds]Pi=S7e*+|oOS*5=<klS;d$o@' );
define( 'AUTH_SALT',        'R! *nP,%p]&78)u09[X]MT=3IxNO)pW`+Dnj}y}qqyR0@!]6f!D8530VHMzEp)E9' );
define( 'SECURE_AUTH_SALT', '}YOR7M;uMDkzDnzpt+u*1,?xQ!^>Fg}]6lOi}$+K(sx(vyjJh^,Y55/UR|qTP,]d' );
define( 'LOGGED_IN_SALT',   '.$!Rn#n s:B^0r-e4_%+pXrt7RxP?r*xjd--*+^xT4Y2`-mrR}!V[HvZ-wIf4pga' );
define( 'NONCE_SALT',       'wtO~jo6uuWj)M`d@Q{#%5<@$}<CHRxA2v>8ETe}mv]D8._.T)[J.oY~O|qGTR3.r' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_tienda';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
